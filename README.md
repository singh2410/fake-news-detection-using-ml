# Fake news detection using TfidfVectorizer & various ML algorithms.
# By- Aarush Kumar
Detecting fake news using various techniques as TfidfVectorizer, LogisticRegression, DecisionTree & RFC.
This python project of detecting fake news deals with fake and real news. Using sklearn, we build a TfidfVectorizer on our dataset. Then, we initialize a PassiveAggressive Classifier and fit the model. In the end, the accuracy score and the confusion matrix tell us how well our model fares.
You’ll need to install the following libraries with pip:numpy pandas & sklearn.
Thankyou!
