#!/usr/bin/env python
# coding: utf-8

# # Fake news detection using variuos ML algorithms.
# #By- Aarush Kumar
# #Dated: May 21,2021

# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
import re
import string


# In[2]:


df_fake = pd.read_csv(r"/home/aarush100616/Downloads/Projects/Fake News Detection/Fake.csv")
df_true = pd.read_csv(r"/home/aarush100616/Downloads/Projects/Fake News Detection/True.csv")


# In[3]:


df_fake


# In[4]:


df_true


# In[5]:


df_fake.head(5)


# In[6]:


df_true.head(5)


# In[7]:


#Inserting column for fake and real news dataset to categories fake and true news.
df_fake["class"] = 0
df_true["class"] = 1


# In[8]:


#Removing last 10 rows from both the dataset, for manual testing
df_fake.shape, df_true.shape


# In[9]:


df_fake_manual_testing = df_fake.tail(10)
for i in range(23480,23470,-1):
    df_fake.drop([i], axis = 0, inplace = True)
df_true_manual_testing = df_true.tail(10)
for i in range(21416,21406,-1):
    df_true.drop([i], axis = 0, inplace = True)


# In[10]:


df_fake.shape, df_true.shape


# In[11]:


#Merging the manual testing dataframe in single dataset and save it in a csv file
df_fake_manual_testing["class"] = 0
df_true_manual_testing["class"] = 1


# In[12]:


df_fake_manual_testing.head(10)


# In[13]:


df_true_manual_testing.head(10)


# In[14]:


df_manual_testing = pd.concat([df_fake_manual_testing,df_true_manual_testing], axis = 0)
df_manual_testing.to_csv("/home/aarush100616/Downloads/Projects/Fake News Detection/testing.csv")


# In[15]:


#Merging the fake and true dataframe
df_marge = pd.concat([df_fake, df_true], axis =0 )
df_marge.head(10)


# In[16]:


df_marge.columns


# In[17]:


#drop the columns
df = df_marge.drop(["title", "subject","date"], axis = 1)


# In[18]:


df.isnull().sum()


# In[19]:


#Creating a function to convert the text in lowercase, remove the extra space,url,links,etc.
def wordopt(text):
    text = text.lower()
    text = re.sub('\[.*?\]', '', text)
    text = re.sub("\\W"," ",text) 
    text = re.sub('https?://\S+|www\.\S+', '', text)
    text = re.sub('<.*?>+', '', text)
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    text = re.sub('\n', '', text)
    text = re.sub('\w*\d\w*', '', text)    
    return text


# In[20]:


df["text"] = df["text"].apply(wordopt)


# In[21]:


#Defining dependent and independent variable as x and y
x = df["text"]
y = df["class"]


# In[22]:


#Splitting the dataset into training set and testing set.
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)


# In[23]:


#Convert text to vectors
from sklearn.feature_extraction.text import TfidfVectorizer
vectorization = TfidfVectorizer()
xv_train = vectorization.fit_transform(x_train)
xv_test = vectorization.transform(x_test)


# In[24]:


#Logistic Regression
from sklearn.linear_model import LogisticRegression
LR = LogisticRegression()
LR.fit(xv_train,y_train)


# In[25]:


pred_lr=LR.predict(xv_test)
LR.score(xv_test, y_test)


# In[26]:


print(classification_report(y_test, pred_lr))


# In[27]:


#Decision tree
from sklearn.tree import DecisionTreeClassifier
DT = DecisionTreeClassifier()
DT.fit(xv_train, y_train)


# In[28]:


pred_dt = DT.predict(xv_test)
DT.score(xv_test, y_test)


# In[29]:


print(classification_report(y_test, pred_dt))


# In[30]:


#RandomForestClassifier
from sklearn.ensemble import RandomForestClassifier
RFC = RandomForestClassifier(random_state=0)
RFC.fit(xv_train, y_train)


# In[31]:


pred_rfc = RFC.predict(xv_test)
RFC.score(xv_test, y_test)


# In[32]:


print(classification_report(y_test, pred_rfc))


# ### Model testing with manual dataset

# In[33]:


def output_lable(n):
    if n == 0:
        return "Fake News"
    elif n == 1:
        return "True News"
    
def manual_testing(news):
    testing_news = {"text":[news]}
    new_def_test = pd.DataFrame(testing_news)
    new_def_test["text"] = new_def_test["text"].apply(wordopt) 
    new_x_test = new_def_test["text"]
    new_xv_test = vectorization.transform(new_x_test)
    pred_LR = LR.predict(new_xv_test)
    pred_DT = DT.predict(new_xv_test)
    pred_RFC = RFC.predict(new_xv_test)
    return print("\n\nLR Prediction: {} \nDT Prediction: {} \nRFC Prediction: {}".format(output_lable(pred_LR[0]), 
                                                                                                              output_lable(pred_DT[0]), 
                                                                                                              output_lable(pred_RFC[0])))


# In[35]:


news = str(input())
manual_testing(news)

